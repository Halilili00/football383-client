import React, { useEffect, useState } from 'react'
import { Row } from 'reactstrap'
import FeedbackForm from '../components/FeedbackForm'
import FeedbackList from '../components/FeedbackList'
import Loading from '../components/Loading'
import { getFeedbacks, postFeedback, removeFeedback } from '../controllers/FeedbackController'
import { getTeams } from '../controllers/TeamController'
import useLocalStorage from '../useLocalStorage'

const containerStyle = {
    formRow: {
        "borderRadius": 20,
        "backgroundColor": "yellow",
        "marginBottom": 20,
        "marginTop": 10
    }
}

const FeedbackContainer = () => {

    const [feedbacks, setFeedbacks] = useState(null);
    const [feedback, setFeedback] = useState({});
    const [teams, setTeams] = useState([]);
    const [message, setMessage] = useState("");
    const [charCounter, setCharCounter] = useState(400);
    const [myFeedbacks, setMyFeedbacks] = useLocalStorage("myfeedbacks", [""])
    const [isPending, setIsPending] = useState(true);


    useEffect(() => {
        initData();
    }, [])

    async function initData() {
        getFeedbacks()
            .then(data => {
                setFeedbacks(data);
                setIsPending(false)
            })
            .catch((error) => {
                alert('Error: ', error);
            });
        getTeams()
            .then(data => {
                setTeams(data);
            })
            .catch((error) => {
                alert('Error: ', error);
            });
    }

    function getcurrenDay() {
        let toDay = new Date();
        return toDay.toLocaleString();
    }

    const handleFormChange = (event) => {
        let feedbackNew = { ...feedback };
        feedbackNew[event.target.name] = event.target.value;
        feedbackNew.commentdate = getcurrenDay();
        setFeedback(feedbackNew);
    }

    const handleCommentChage = (event) => {
        let feedbackNew = { ...feedback };
        feedbackNew[event.target.name] = event.target.value;
        setCharCounter(400 - feedbackNew.comment.length)
        setFeedback(feedbackNew);
    }


    const handleSubmit = (event) => {
        if (feedback.commentdate && feedback.comment && feedback.username && feedback.teamname) {
            setMessage("");
            postFeedback(feedback)
            //event.preventDefault();
            alert(JSON.stringify(feedback))
            if (myFeedbacks) {
                setMyFeedbacks([...myFeedbacks, feedback]);
            }
            else {
                setMyFeedbacks([feedback])
            }

        }
        else {
            event.preventDefault();
            setMessage("All date is needed")
        }
    }

    const deleteFeedback = (feedback) => {
        if (myFeedbacks.find(mf => mf.commentdate === feedback.commentdate)) {
            removeFeedback(feedback)
            let newMyFeedbacks = myFeedbacks.filter(mf => mf.commentdate !== feedback.commentdate)
            setMyFeedbacks(newMyFeedbacks)
            alert("Feedback is deleted successfully")
            let newFeedbacks = feedbacks.filter(f => f.id !== feedback.id);
            setFeedbacks(newFeedbacks);
        }
        else {
            alert("You can't delete this feedback, because this is not your feedback!")
        }
    }

    return (
        <div>
            <Row style={containerStyle.formRow}>
                <FeedbackForm
                    teams={teams}
                    submit={handleSubmit}
                    feedback={feedback}
                    change={handleFormChange}
                    changeComment={handleCommentChage}
                    charCounter={charCounter}
                />
                {message ? message : ""}
            </Row>
            <Row>
                {isPending && <Loading/>}
                {feedbacks && <FeedbackList
                    feedbacks={feedbacks}
                    deleteFeedback={deleteFeedback}
                />}
            </Row>
        </div>
    )
}

export default FeedbackContainer
