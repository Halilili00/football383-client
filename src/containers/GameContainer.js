import React, { useEffect, useState } from 'react'
import { Col, Row, Button } from 'reactstrap';
import GameList from '../components/GameList';
import { getGames, getGamesWithLeague, getGamesWithSeason, getGamesWithTeamName } from '../controllers/GameController';
import { getLeagues } from '../controllers/LeagueController';
import LeagueList from '../components/LeagueList';
import { MdOutlineFeedback } from "react-icons/md";
import Loading from '../components/Loading';

const appStyle = {
    "position": "fixed",
    "right": "55px",
    "bottom": "25px",
    "width": "26%",
    "fontSize": 25
}

const GameContainer = () => {

    const [games, setGames] = useState(null);
    const [leagues, setLeagues] = useState(null);
    const [currenLeague, setCurrenLeague] = useState("");
    const [currenTeam, setCurrenTeam] = useState("");
    const [currentSeason, setCurrenSeason] = useState("");
    const [isPending, setIsPending] = useState(true);

    const handleChange = (event) => {
        let searchTeam = event.target.value;
        setCurrenTeam(searchTeam);
    }

    const changeTeam = (team) => {
        setIsPending(true)
        setGames(null)
        if (currenLeague) {
            setCurrenLeague("");
        }
        setCurrenTeam(team)
        getGamesWithTeamName(team)
            .then(data => {
                if (currentSeason) {
                    setGames(data.filter(d => d.season === currentSeason));
                }
                else {
                    setGames(data);
                }
                setIsPending(false)
            })
            .catch((error) => {
                alert('Error: ', error);
            });
    }

    const changeleague = (league) => {
        setIsPending(true)
        setGames(null)
        setCurrenTeam("")
        setCurrenLeague(league);
        getGamesWithLeague(league.leaguecode)
            .then(data => {
                if (currentSeason) {
                    setGames(data.filter(d => d.season === currentSeason))
                }
                else {
                    setGames(data);
                }
                setIsPending(false)
            })
            .catch((error) => {
                alert('Error: ', error);
            });
    }

    useEffect(() => {
        changeSeason(currentSeason)
    }, [currentSeason])

    const changeSeason = (season) => {
        setIsPending(true)
        setGames(null)
        setCurrenSeason(season);
        if (currenLeague) {
            changeleague(currenLeague);
        }
        else if (currenTeam) {
            changeTeam(currenTeam);
        }
        else {
            getGamesWithSeason(season)
            .then(data => {
                setGames(data);
                setIsPending(false)
            })
            .catch((error) => {
                alert('Error: ', error);
            });
        }
    }

    useEffect(() => {
        initData();
    }, [])

    async function initData() {
        getGames()
            .then(data => {
                setGames(data);
            })
            .catch((error) => {
                alert('Error: ', error);
            });
        getLeagues()
            .then(data => {
                setLeagues(data);
            })
            .catch((error) => {
                alert('Error: ', error);
            });
    }

    const show = (currentSeason).slice(2, 4) + "/" + (currentSeason).slice(7, 9)
    return (
        <div>
            <Row>
                <Col xs={9}>
                    <h3 style={{ "textAlign": "left", "backgroundColor": "#F0F8FF" }}>
                        {currenLeague.leaguename} {(currentSeason) ? `- ${show}` : ""}
                    </h3>
                    { isPending && <Loading/>}
                    { games && <GameList
                        games={games}
                        changeSeason={changeSeason}
                    />}
                </Col>
                <Col xs={3}>
                    { leagues && <LeagueList
                        leagues={leagues}
                        changeTeam={changeTeam}
                        handleChange={handleChange}
                        currenTeam={currenTeam}
                        currenLeague={currenLeague}
                        changeleague={changeleague}
                    />}
                </Col>
            </Row>
            <Button style={appStyle} href="/feedback">Give Feedback <MdOutlineFeedback/></Button>
        </div>
    )
}

export default GameContainer
