import { Container, Row } from 'reactstrap';
import './App.css';
import Navi from './components/Navi';
import GameContainer from './containers/GameContainer';
import NotFound from './components/NotFound';
import { Route } from 'react-router-dom';
import { Routes } from 'react-router-dom';
import FeedbackContainer from './containers/FeedbackContainer';

function App() {

  return (
    <div className="App">
      <Container>
        <Row>
          <Navi />
        </Row>
        <Row>
          <Routes>
            <Route path='/' element={
              <GameContainer />
            } />
            <Route path='feedback' element={
              <FeedbackContainer />
            } />
            <Route path='*' element={<NotFound />} />
          </Routes>
        </Row>
      </Container>
    </div>
  );
}

export default App;
