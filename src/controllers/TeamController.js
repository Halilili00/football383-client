import settings from './settings.json';

export async function getTeams() {
    let baseUrl = settings.urlTeams;
    
    return fetch(baseUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': "Basic " + btoa(settings.auth)
        }
    })
        .then(response => response.json())
        .then(data => { return (data) });
}