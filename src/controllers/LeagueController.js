import settings from './settings.json';

export async function getLeagues() {
    let baseUrl = settings.urlLeagues;

    return fetch(baseUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': "Basic " + btoa(settings.auth)
        }
    })
        .then(response => response.json())
        .then(data => { return (data) });
}