import settings from './settings.json';

export async function getGames() {
    let baseUrl = settings.urlMatches;

    return fetch(baseUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': "Basic " + btoa(settings.auth)
        }
    })
        .then(response => response.json())
        .then(data => { return (data) });
}

export async function getGamesWithLeague(currenLeague) {
    let baseUrl = settings.urlMatches;
    if (currenLeague) {
        baseUrl += "/league/" + currenLeague;
    }

    return fetch(baseUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': "Basic " + btoa(settings.auth)
        }
    })
        .then(response => response.json())
        .then(data => { return (data) });
}

export async function getGamesWithTeamName(currenTeam) {
    let baseUrl = settings.urlMatches;
    if (currenTeam) {
        baseUrl += "/" + currenTeam;
    }

    return fetch(baseUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': "Basic " + btoa(settings.auth)
        }
    })
        .then(response => response.json())
        .then(data => { return (data) });
}

export async function getGamesWithSeason(currenSeason) {
    let baseUrl = settings.urlMatches
    if (currenSeason) {
        baseUrl += "/season/" + currenSeason;
        console.log(baseUrl)
    }

    return fetch(baseUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': "Basic " + btoa(settings.auth)
        }
    })
        .then(response => response.json())
        .then(data => { return (data) });
}
