import settings from './settings.json';

export async function getFeedbacks() {
    let baseUrl = settings.urlFeedback + "feedbacks"

    return fetch(baseUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': "Basic " + btoa(settings.auth)
        }
    })
        .then(response => response.json())
        .then(data => { return (data) });
}

export async function postFeedback(newFeedback) {
    let baseUrl = settings.urlFeedback + "feedbackpost"

    const response = await fetch(baseUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': "Basic " + btoa(settings.auth)
        },
        body: JSON.stringify(newFeedback)
    })
    return response.data;
}

export async function removeFeedback(feedback) {
    let baseUrl = settings.urlFeedback + "feedback";

    fetch(baseUrl, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': "Basic " + btoa(settings.auth)
        },
        body: JSON.stringify(feedback)
    })
        .then(() => {
            console.log("Deleted");
        })
}

