import React from "react"
import ReactDOM from "react-dom"
import { mount, configure } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import FeedbackList from "../components/FeedbackList"

const testFeedback = [
    {
        "id": 1,
        "username": "Test1",
        "commentdate": "08.05.2022 11:02:14",
        "teamname": "Arsenal",
        "comment": "Test feedback"
    },
    {
        "id": 2,
        "username": "Test2",
        "commentdate": "08.05.2022 11:06:52",
        "teamname": "Arsenal",
        "comment": "Test feedback"
    },
]

configure({ adapter: new Adapter() });

describe("Testting FeedbackList component", () => {
    it("Should render without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<FeedbackList feedbacks={testFeedback.length} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it("The table exists", () => {
        const wrapper = mount(<FeedbackList feedbacks={testFeedback} />);
        expect(wrapper.find("div").find(".container").length == 1).toBe(true)
    })

    it("Table has 2 row", () => {
        const wrapper = mount(<FeedbackList feedbacks={testFeedback} />);
        expect(wrapper.find("div").find(".row.dataTable").length == 2).toBe(true)
    })

    it("The first table data of table sould be name \"Test1\"", () => {
        const wrapper = mount(<FeedbackList feedbacks={testFeedback} />);
        expect(wrapper.find(".col-sm-3.useName").first().text() == "Test1").toBe(true)
    })

    it("The second table data of table sould be favorite teamname \"Favorite team: Arsenal\"", () => {
        const wrapper = mount(<FeedbackList feedbacks={testFeedback} />);
        expect(wrapper.find(".col-sm-8.teamName").first().text() == "Favorite team: Arsenal").toBe(true)
    })

    it("The third table data of table sould show delete button", () => {
        const wrapper = mount(<FeedbackList feedbacks={testFeedback} />);
        expect(wrapper.find(".col-sm-1.deletebutton").first().length == 1).toBe(true)
    })

    it("The fourth table data of table sould be comment", () => {
        const wrapper = mount(<FeedbackList feedbacks={testFeedback} />);
        console.log(wrapper.find(".col-sm-8.teamName").first().text())
        expect(wrapper.find(".col-sm-9.usercomment").first().text() == "Test feedback").toBe(true)
    })

    it("Lastly last table data of table sould be show when feedback is posted", () => {
        const wrapper = mount(<FeedbackList feedbacks={testFeedback} />);
        console.log(wrapper.find(".col-sm-12.commentdate").first().text())
        expect(wrapper.find(".col-sm-12.commentdate").first().text() == "Added: 08.05.2022 11:02:14").toBe(true)
    })
})