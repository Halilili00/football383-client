import React from "react"
import ReactDOM from "react-dom"
import { mount, configure } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import GameList from "../components/GameList"

const testGames = [
  {
    "id": 1,
    "matchnumber": 1,
    "roundnumber": 1,
    "date": "2021-08-13 19:00:00Z",
    "location": "Brentford Community Stadium",
    "hometeam": "Brentford",
    "awayteam": "Arsenal",
    "hometeamscore": 2,
    "awayteamscore": 0,
    "season": "2021-2022",
    "leaguecode": "PL"
  },
  {
    "id": 2,
    "matchnumber": 2,
    "roundnumber": 1,
    "date": "2021-08-14 11:30:00Z",
    "location": "Old Trafford",
    "hometeam": "Man Utd",
    "awayteam": "Leeds",
    "hometeamscore": 5,
    "awayteamscore": 1,
    "season": "2021-2022",
    "leaguecode": "PL"
  },
]

configure({ adapter: new Adapter() });

describe("Testting GameList component", () => {
  it("Should render without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<GameList games={testGames} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("The table exists", () => {
    const wrapper = mount(<GameList games={testGames} />);
    expect(wrapper.find("div").find(".table-bordered.table").length == 1).toBe(true)
  })

  it("Table has 3 row. 1 for table headers and 2 other is for data", () => {
    const wrapper = mount(<GameList games={testGames} />);
    expect(wrapper.find("div").find("tr").length == 3).toBe(true)
  })

  it("The row has seven column", () => {
    const wrapper = mount(<GameList games={testGames} />);
    //Divide with 2, because we have two row with td
    expect(wrapper.find("div").find("td").length/2 == 7).toBe(true)
  })

  it("The table first column data sould be 1", () => {
    const wrapper = mount(<GameList games={testGames} />);
    expect(wrapper.find("div").find("td").first().text() == "1").toBe(true)
  })

  it("The table second column data sould be 2021-08-13 19:00", () => {
    const wrapper = mount(<GameList games={testGames} />);
    expect(wrapper.find("div").find("td").at(1).text() == "2021-08-13 19:00").toBe(true)
  })

  it("The table third column data sould be Brentford", () => {
    const wrapper = mount(<GameList games={testGames} />);
    expect(wrapper.find("div").find("td").at(2).text() == "Brentford").toBe(true)
  })

  it("The table fourth column data sould be 2", () => {
    const wrapper = mount(<GameList games={testGames} />);
    expect(wrapper.find("div").find("td").at(3).text() == "2").toBe(true)
  })

  it("The table fifth column data sould be - ", () => {
    const wrapper = mount(<GameList games={testGames} />);
    console.log(wrapper.find("div").find("td").at(4).text())
    expect(wrapper.find("div").find("td").at(4).text() == " - ").toBe(true)
  })

  it("The table sixth column data sould be 0", () => {
    const wrapper = mount(<GameList games={testGames} />);
    expect(wrapper.find("div").find("td").at(5).text() == "0").toBe(true)
  })

  it("The table last column data sould be Arsenal", () => {
    const wrapper = mount(<GameList games={testGames} />);
    expect(wrapper.find("div").find("td").at(6).text() == "Arsenal").toBe(true)
  })
})