import React from "react"
import ReactDOM from "react-dom"
import { mount, configure } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import LeagueList from "../components/LeagueList";

const testLeagues= [
    {
        "id":1,
        "leaguename": "Premier League",
        "leaguecode": "PL"
    },
    {
        "id":2,
        "leaguename": "Bundesliga",
        "leaguecode": "BL"
    },
]

configure({ adapter: new Adapter() });

describe("Testting LeagueList component", () => {
    it("Should render without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<LeagueList leagues={testLeagues} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it("The table exists", () => {
        const wrapper = mount(<LeagueList leagues={testLeagues} />);
        expect(wrapper.find("div").find(".list-group").length == 1).toBe(true)
    })

    it("Table has 3 row", () => {
        const wrapper = mount(<LeagueList leagues={testLeagues} />);
        //3 beacause we have 2 data item and 1 for searching
        expect(wrapper.find("div").find(".list-group-item").length == 3).toBe(true)
    })

    it("The first data of table sould be search input", () => {
        const wrapper = mount(<LeagueList leagues={testLeagues} />);
        expect(wrapper.find(".list-group-item").first().find(".searchInput").length == 1).toBe(true)
    })

    it("The second data of table sould be Premier League", () => {
        const wrapper = mount(<LeagueList leagues={testLeagues} />);
        expect(wrapper.find(".list-group-item").at(1).text() == "Premier League").toBe(true)
    })
})