import React from 'react'
import { Button, Col, Container, Row } from 'reactstrap'
import NotFound from './NotFound'
import { BsTrashFill } from "react-icons/bs";

const listStyle = {
    Container: {
        "textAlign": "left",
    },
    h2: {
        "textAlign": "left",
        "backgroundColor": "#FFD700"
    },
    Row: {
        "marginBottom": 35,
        "backgroundColor": "#FDF5E6",
        "borderRadius": 10,
        "fontSize": 20
    },
    Button: {
        "fontSize": 5, 
    }
}

const FeedbackList = (props) => {
    return (
        <div>
            <h2 style={listStyle.h2}>Your feedbacks</h2>
            {props.feedbacks.length > 0
                ? <Container style={listStyle.Container}>
                    {props.feedbacks?.map?.(feedback => (
                        <Row className="dataTable" key={feedback.id} style={listStyle.Row}>
                            <Row className="first">
                                <Col className="useName" sm={3}>
                                    <h4>{feedback.username}</h4>
                                </Col>
                                <Col className="teamName" sm={8}>
                                    <h5>Favorite team: {feedback.teamname}</h5>
                                </Col>
                                <Col className="deletebutton" sm={1} style={listStyle.Button}>
                                    <Button onClick={() => props.deleteFeedback(feedback)}><BsTrashFill/></Button>
                                </Col>
                            </Row>
                            <Row className="second">
                                <Col className="comment" sm={3}>
                                    <h4>Comment:</h4>
                                </Col>
                                <Col className="usercomment" sm={9} style={{ "backgroundColor": "#F8F8FF" }}>
                                    {feedback.comment}
                                </Col>
                            </Row>
                            <Row className="third" style={{ "textAlign": "right", "fontSize": 13 }} sm={12}>
                                <Col className="commentdate" sm={12}>
                                    Added: {feedback.commentdate}
                                </Col>
                            </Row>
                        </Row>
                    ))}
                </Container>
                : <NotFound />}
        </div>
    )
}

export default FeedbackList
