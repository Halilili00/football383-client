import React from 'react'
import { Container, Nav, Navbar, NavbarBrand, NavItem, NavLink } from 'reactstrap'
import { MdOutlineFeedback } from "react-icons/md";
import { RiFootballFill } from "react-icons/ri";

const Navi = () => {


    return (
        <div>
            <Navbar variant="dark" style={{ 'background': 'black', "fontFamily": "Georgia" }}>
                <Container>
                    <Nav className="me-auto" >
                        <NavbarBrand href="/">F<RiFootballFill/><RiFootballFill/>TBALL</NavbarBrand>
                        <NavLink href="/">Home</NavLink>
                        <NavItem style={{ 'marginLeft': 'auto' }}>
                            <NavLink href="/feedback" >Feedbacks <MdOutlineFeedback/></NavLink>
                        </NavItem>
                    </Nav>
                </Container>
            </Navbar>
        </div>
    )
}

export default Navi
