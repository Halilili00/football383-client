import React from 'react'
import { Nav, NavItem, NavLink, Table } from 'reactstrap';
import NotFound from "../components/NotFound"

const navStyle = {
    "backgroundColor": "#DC143C"
}

const GameList = (props) => {

    return (
        <div>
            <Nav fill variant="tabs" style={{ "backgroundColor": "#FFF8DC" }}>
                <NavItem style={navStyle}>
                    <NavLink onClick={() => props.changeSeason("")}>All</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink onClick={() => props.changeSeason("2021-2022")}>2021/2022</NavLink>
                </NavItem>
                <NavItem className="justify-content-end">
                    <NavLink onClick={() => props.changeSeason("2020-2021")}>2020/2021</NavLink>
                </NavItem>
            </Nav>
            {props.games.length > 0
                ? <Table bordered>
                    <thead>
                        <tr>
                            <>
                                <th>Matchnum</th>
                                <th>Date    </th>
                                <th>hometeam</th>
                                <th>hometeamscore</th>
                                <th> - </th>
                                <th>awayteamscore</th>
                                <th>awayteam</th>
                            </>
                        </tr>
                    </thead>
                    <tbody>
                        {props.games.map(game => (
                            <tr key={game.id}>
                                <td>{game.matchnumber}</td>
                                <td>{(game.date).slice(0, 16)}</td>
                                <td>{game.hometeam}</td>
                                <td>{game.hometeamscore}</td>
                                <td> - </td>
                                <td>{game.awayteamscore}</td>
                                <td>{game.awayteam}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
                : <NotFound />}
        </div>
    )
}

export default GameList
