import React from 'react'

const NotFound = () => {
  return (
    <div>
      <h2 style={{textAlign:"center", "marginTop": "40%"}}>Not Found</h2>
    </div>
  )
}

export default NotFound
