import React from 'react'
import { Button, ListGroup, ListGroupItem } from 'reactstrap'

const LeagueList = ({ currenTeam, handleChange, changeTeam, changeleague, leagues }) => {

    return (
        <div>
            <h2 onClick={() => changeleague("")}>Leagues</h2>
            <ListGroup>
                <ListGroupItem>
                    <input className="searchInput" type="text" name="team" placeholder='Search with team name' onChange={event => handleChange(event)} value={currenTeam}></input>
                    <Button onClick={() => changeTeam(currenTeam)} color="danger" style={{ 'marginLeft': 10 }}>Search</Button>
                </ListGroupItem>
                {leagues.map(league => (
                    <ListGroupItem
                        onClick={() => changeleague(league)}
                        key={league.id}
                    >
                        {league.leaguename}</ListGroupItem>
                ))}
            </ListGroup>
        </div>
    )
}

export default LeagueList
