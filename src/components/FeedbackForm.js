import React from 'react'
import { Col, Form, FormGroup, Input, Label, Row, Button } from 'reactstrap'

const formStyle = {
  h2: {
    "textAlign": "left",
    "backgroundColor": "#FFD700",
    "borderRadius": 5,
    "marginBottom": 15,
    "marginLeft": -10,
    "marginRight": -10
  },
  button: {
    'marginBottom': 10,
    "width": "100%"
  }
}

const FeedbackForm = (props) => {

  return (
    <div>
      <h2 style={formStyle.h2}>Give us feedback</h2>
      <Form onSubmit={props.submit}>
        <Row>
          <Col sm={4}>
            <FormGroup row>
              <Label for="username" sm={3}>Username</Label>
              <Col sm={9}>
                <Input type='text' name="username" placeholder='Enter your name' onChange={props.change}></Input>
              </Col>
            </FormGroup>
          </Col>
          <Col sm={8}>
            <FormGroup row>
              <Label for="teamname" sm={3}>Pick your favorite team</Label>
              <Col sm={9}>
                <Input type='select' name="teamname" placeholder='Enter teamname' onChange={props.change}>
                  <option></option>
                  {props.teams.map(team => (
                    <option key={team.id}>{team.teamname}</option>
                  ))}
                </Input>
              </Col>
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="comment" style={{ "textAlign": "left" }} sm={10}>Comment</Label>
          <Label sm={2}>Max length: {props.charCounter}/400</Label>
          <Input type='textarea' name="comment" placeholder='Enter comment' onChange={props.changeComment}></Input>
        </FormGroup>
        <Button className='submitButton' color="danger" type='submit' style={formStyle.button}>Submit</Button>
      </Form>
    </div>
  )
}

export default FeedbackForm
