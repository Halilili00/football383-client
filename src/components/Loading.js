import React from 'react'
import ReactLoading from "react-loading";
import { Col, Row } from 'reactstrap';

const Loading = () => {
  return (
    <div>
        <Row style={{"fontSize": 50, "marginTop": "20%", "marginLeft": "40%", "fontFamily": "Courier New"}}>
        <Col sm={6}>
        Loading 
        </Col>
        <Col style={{"marginLeft":"-8%"}} sm={6}>
        <ReactLoading type="spokes" color="#0000FF"
        height={100} width={50} />
        </Col>
        </Row>
    </div>
  )
}

export default Loading
